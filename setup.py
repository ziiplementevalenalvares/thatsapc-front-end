from distutils.core import setup
import py2exe

setup(name="ThatsaPC",
      version="0.1",
      author="javazkript@gmail.com",
      author_email="thatsapc@javazkript.com",
      url="www.javazkript.com",
      license="Creative Commons by-nc-sa",
      packages=['code'],
      package_dir={'code': '.'},
      package_data={"code": ["html/*","binaries/*"]},
      scripts=["Login.py"],
      windows=[{"script": "Login.py"}],
      options={"py2exe": {"skip_archive": True, "includes": ["sip"]}})
